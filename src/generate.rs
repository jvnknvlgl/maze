use crate::{Maze, Pos, CELL_SIZE, MAZE_SIZE, PATH_CHAR, TRAV_CHAR, WALL_CHAR};
use rand::prelude::ThreadRng;
use rand::Rng;

pub fn generate() -> (i32, Maze) {
    let mut rng: ThreadRng = rand::thread_rng();
    let mut maze = [[WALL_CHAR; MAZE_SIZE as usize]; MAZE_SIZE as usize];

    maze[2 * rng.gen_range(0..CELL_SIZE as usize) + 1][0] = PATH_CHAR;
    maze[2 * rng.gen_range(0..CELL_SIZE as usize) + 1][MAZE_SIZE as usize - 1] = TRAV_CHAR;

    let mut pos = Pos {
        x: 2 * rng.gen_range(0..CELL_SIZE) + 1,
        y: 2 * rng.gen_range(0..CELL_SIZE) + 1,
    };

    maze[pos.x as usize][pos.y as usize] = PATH_CHAR;

    let mut total_visited = 1;
    let mut iter = 0;
    let mut stuck_count = 0;

    while total_visited < CELL_SIZE * CELL_SIZE {
        iter += 1;

        let pos_next = match rng.gen_range(0..4) {
            0 => Pos {
                x: pos.x + 2,
                y: pos.y,
            },
            1 => Pos {
                x: pos.x,
                y: pos.y + 2,
            },
            2 => Pos {
                x: pos.x - 2,
                y: pos.y,
            },
            _ => Pos {
                x: pos.x,
                y: pos.y - 2,
            },
        };

        if pos_next.x > MAZE_SIZE - 2
            || pos_next.y > MAZE_SIZE - 2
            || pos_next.x < 1
            || pos_next.y < 1
        {
            continue;
        }

        if maze[pos_next.x as usize][pos_next.y as usize] == WALL_CHAR {
            maze[pos_next.x as usize][pos_next.y as usize] = PATH_CHAR;
            maze[((pos.x + pos_next.x) / 2) as usize][((pos.y + pos_next.y) / 2) as usize] =
                PATH_CHAR;

            total_visited += 1;
            pos = pos_next;

            continue;
        }

        if stuck_count < 8 {
            stuck_count += 1;

            continue;
        }

        stuck_count = 0;

        loop {
            pos.x = 2 * rng.gen_range(0..CELL_SIZE) + 1;
            pos.y = 2 * rng.gen_range(0..CELL_SIZE) + 1;

            if maze[pos.x as usize][pos.y as usize] == PATH_CHAR {
                break;
            }
        }
    }

    (iter, maze)
}
