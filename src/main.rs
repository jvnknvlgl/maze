mod generate;
mod traverse;

use crate::generate::generate;
use crate::traverse::traverse;

const WALL_CHAR: char = '#';
const PATH_CHAR: char = ' ';
const TRAV_CHAR: char = '.';

const CELL_SIZE: i32 = 8;
const MAZE_SIZE: i32 = 2 * CELL_SIZE + 1;

struct Pos {
    x: i32,
    y: i32,
}

type Maze = [[char; MAZE_SIZE as usize]; MAZE_SIZE as usize];

fn print_maze(maze: Maze) {
    for y in maze.iter() {
        for x in y.iter() {
            print!("{x} ");
        }

        println!();
    }

    println!();
}

fn find_start_pos(maze: Maze) -> Pos {
    for (i, x) in maze.iter().enumerate() {
        for (j, _y) in x.iter().enumerate() {
            if maze[i][j] == TRAV_CHAR {
                return Pos {
                    x: i as i32,
                    y: j as i32,
                };
            }
        }
    }

    Pos { x: 0, y: 0 }
}

fn main() {
    println!("\nTool to generate and solve mazes");
    println!("Copyright (c) 2020-2024 Jasper Vinkenvleugel\n");

    let (iter, mut maze) = generate();

    println!(
        "Maze generated in {iter} (out of {}) iterations:\n",
        CELL_SIZE * CELL_SIZE
    );

    print_maze(maze);

    let pos = find_start_pos(maze);

    let pos_check = if pos.x == MAZE_SIZE - 1 {
        Pos { x: 0, y: 1 }
    } else {
        Pos { x: 1, y: 0 }
    };

    if traverse(&mut maze, pos, pos_check) {
        println!("The maze is solved!\n");
        print_maze(maze);
    } else {
        println!("The maze cannot be solved!\n");
    }
}
