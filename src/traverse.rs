use crate::{Maze, Pos, MAZE_SIZE, TRAV_CHAR, WALL_CHAR};

pub fn traverse(maze: &mut Maze, mut pos: Pos, pos_check: Pos) -> bool {
    let pos_next: Pos;

    if maze[(pos.x + pos_check.y) as usize][(pos.y - pos_check.x) as usize] != WALL_CHAR {
        pos_next = Pos {
            x: pos_check.y,
            y: -pos_check.x,
        }
    } else if maze[(pos.x + pos_check.x) as usize][(pos.y + pos_check.y) as usize] != WALL_CHAR {
        pos_next = Pos {
            x: pos_check.x,
            y: pos_check.y,
        }
    } else if maze[(pos.x - pos_check.y) as usize][(pos.y + pos_check.x) as usize] != WALL_CHAR {
        pos_next = Pos {
            x: -pos_check.y,
            y: pos_check.x,
        }
    } else if maze[(pos.x - pos_check.x) as usize][(pos.y - pos_check.y) as usize] != WALL_CHAR {
        pos_next = Pos {
            x: -pos_check.x,
            y: -pos_check.y,
        }
    } else {
        return false;
    }

    pos.x += pos_next.x;
    pos.y += pos_next.y;

    maze[pos.x as usize][pos.y as usize] = TRAV_CHAR;

    if pos.x == 0 || pos.y == 0 || pos.x == MAZE_SIZE - 1 || pos.y == MAZE_SIZE - 1 {
        return true;
    }

    traverse(maze, pos, pos_next)
}
