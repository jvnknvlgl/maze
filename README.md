# Maze generator and solver

```
# # # # # # # # # # # # # # # # #
# . # . . . # . # . # . . . . . #
# . # . # . # . # . # . # # # # #
# . # . # . . . # . . . # . . . #
# . # . # # # . # . # # # # # . #
# . . . . . # . # . # . . . . . #
# . # # # . # . # . # # # # # . #
# . . . # . # . # . # . . . . . .
# . # # # # # # # . # . # # # # #
# . . . . . . . . . . .         #
# . # # # # # # #   # # # # #   #
# . #           #       #       #
# . #   #   #   # # #   # # # # #
# . . . #   #   #   #           #
# # # . #   #   #   # # # # #   #
. . . . #   #       #           #
# # # # # # # # # # # # # # # # #
```

Iterative maze generator and recursive maze solver using the right hand method. Maze size can be determined at compile-time. The function `traverse_maze()` is a re-implementation of an older project of mine written in C++, this project helped me get into Rust for the first time.

## Usage

- Building: `cargo build`
- Running: `cargo run`

# TODO

- Add Dijkstra's algorithm as a way to get the best solution for solving the maze.